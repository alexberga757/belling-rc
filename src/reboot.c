#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/reboot.h>
#include <belling-rc.h>

int main() {
    process("halt");
    // Gọi hàm reboot để khởi động lại hệ thống
    if (reboot(RB_AUTOBOOT) == -1) {
        perror("reboot");
        exit(EXIT_FAILURE);
    }

    return 0;
}
