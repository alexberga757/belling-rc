#include <stdio.h>
#include <string.h>
#include <belling-rc.h>

#define LIST_LOAD "etc/init.belling.conf"
#define DIR_LOAD "etc/init.belling.d"

int main(int argc,char* argv[]) {
	int count_list;
    char** list_conf = readLines(LIST_LOAD, &count_list);
    for (int i = 0; i < count_list; i++) {
        char tcmd[1024];
        strcpy(tcmd, DIR_LOAD);
        strcat(tcmd, "/");
        strcat(tcmd, list_conf[i]);

        char* vcmd[] = {tcmd, "stop", NULL};
        process(vcmd);
    }

    // Free the allocated memory for list_conf
    for (int i = 0; i < count_list; i++) {
        free(list_conf[i]);
    }
    free(list_conf);

    return 0;
}