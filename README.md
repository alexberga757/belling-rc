# Belling-RC

## Giới thiệu
Dự án khởi động đơn giản của Linux (lấy cảm hứng từ System V).

## Yêu cầu gói mã nguồn
GNU/C Library

# Xây dựng mã nguồn 
`gcc <tên file trong ./src> -o <tên file nhưng loại bỏ c :D> -I./include`<br>
**Lưu ý**: PWD Đang trỏ tới thư mục `./` 
- -I./include (điều đó giúp gcc xác định được tệp header)

# Cài đặt 
`./include` -> `/usr/include` <br>
`./src` -> `/usr/sbin (hoặc /usr/bin)` (Do đa phần các bản phân phối của Linux các thư mục /bin thực chất chỉ là symlink của /usr/bin)