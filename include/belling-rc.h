#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>  
#include <string.h>


#define MAX_LINE_LENGTH 1024

char** readLines(const char* filename, int* lineCount) {
    FILE* file = fopen(filename, "r");

    if (file == NULL) {
        perror("Error opening file");
        exit(EXIT_FAILURE);
    }

    // Count the number of lines in the file
    int count = 0;
    char buffer[MAX_LINE_LENGTH];
    while (fgets(buffer, sizeof(buffer), file) != NULL) {
        count++;
    }

    // Reset file pointer to the beginning of the file
    fseek(file, 0, SEEK_SET);

    // Allocate memory for an array of strings
    char** lines = (char**)malloc(count * sizeof(char*));

    // Read each line into the array
    for (int i = 0; i < count; i++) {
        lines[i] = (char*)malloc(MAX_LINE_LENGTH * sizeof(char));
        if (fgets(lines[i], MAX_LINE_LENGTH, file) == NULL) {
            perror("Error reading line");
            exit(EXIT_FAILURE);
        }
    }

    // Set the lineCount parameter
    *lineCount = count;

    fclose(file);
    return lines;
}

void process(char* args[]) {
    pid_t pid = fork();

    if (pid == -1) {
        perror("fork failed");
        exit(EXIT_FAILURE);
    }

    if (pid == 0) {
        // This code is executed by the child process
        execvp(args[0], args);
        perror("execvp failed");
        exit(EXIT_FAILURE);
    } else {
        // This code is executed by the parent process
        // You can put any code you want the parent to execute here
    }
}


